import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  currentPage = '';
  constructor(private router:Router, private globalDataService:GlobalDataService) { }

  ngOnInit() {
    this.currentPage = this.globalDataService.data.currentPage;
  }

  ngDoCheck(){
    console.log(this.currentPage);
    this.currentPage = this.globalDataService.data.currentPage;
  }

  navigateTo(page:string, elm:any){
    this.router.navigateByUrl(page);
    /*if(elm){
      console.log(elm.currentTarget);
      elm.currentTarget.classList.add('active');
    }*/
  }

  logout(){
    this.globalDataService.data.validUser = false;
    this.navigateTo('login',null);
  }

}
