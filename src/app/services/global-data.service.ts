import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalDataService {
  initialData = {
    validUser:false,
    userDetails:{
      name:''
    },
    pages:{
      page1:{
        visited:false,
        visitCount:0
      },
      page2:{
        visited:false,
        visitCount:0
      },
      lazy:{
        visited:false,
        visitCount:0
      }
    },
    currentPage:'login'
  };
  data = {
    validUser:false,
    userDetails:{
      name:''
    },
    pages:{
      page1:{
        visited:false,
        visitCount:0
      },
      page2:{
        visited:false,
        visitCount:0
      },
      lazy:{
        visited:false,
        visitCount:0
      }
    },
    currentPage:'login'
  }
  constructor() { }
}
